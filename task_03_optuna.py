import pandas as pd
import optuna
from xgboost import XGBRegressor
from sklearn.metrics import mean_squared_error
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler

# Load the data
df_train = pd.read_csv('task_03/data/internship_train.csv')
df_test = pd.read_csv('task_03/data/internship_hidden_test.csv')

# Split the data into training and validation sets
X_train, X_val, y_train, y_val = train_test_split(df_train.drop('target', axis=1), 
                                                  df_train['target'], 
                                                  test_size=0.2, 
                                                  random_state=42)

# Define the objective function for Optuna to minimize
def objective(trial):
    params = {
        'n_estimators': trial.suggest_categorical('n_estimators', [50, 100, 150]),
        'max_depth': trial.suggest_categorical('max_depth', [3, 5, 7]),
        'learning_rate': trial.suggest_categorical('learning_rate', [0.1, 0.05, 0.01])
    }

    model = XGBRegressor(**params)
    model.fit(X_train, y_train)

    y_pred_val = model.predict(X_val)
    rmse_val = mean_squared_error(y_val, y_pred_val, squared=False)

    return rmse_val

# Run the optimization process
study = optuna.create_study(direction='minimize')
study.optimize(objective, n_trials=20)

# Train the model with the best hyperparameters
best_params = study.best_params
model = XGBRegressor(**best_params)
model.fit(X_train, y_train)

# Evaluate the model
y_pred_train = model.predict(X_train)
y_pred_val = model.predict(X_val)

rmse_train = mean_squared_error(y_train, y_pred_train, squared=False)
rmse_val = mean_squared_error(y_val, y_pred_val, squared=False)

print('Training RMSE:', rmse_train)
print('Validation RMSE:', rmse_val)

df_test.fillna(df_test.mean(), inplace=True)

# Make predictions on the test data
y_pred_test = model.predict(df_test)

# Save predictions to file
submission = pd.DataFrame({'target': y_pred_test})
submission.to_csv('task_03/data/predictions_optuna.csv', index=False)
