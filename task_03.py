import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
from xgboost import XGBRegressor
from sklearn.metrics import mean_squared_error
from sklearn.model_selection import GridSearchCV


# Load the dataset into a pandas dataframe using the read_csv() function:
df_train = pd.read_csv('task_03/data/internship_train.csv')
df_test = pd.read_csv('task_03/data/internship_hidden_test.csv')

# Display the first 5 rows of the dataframe:
print(df_train.head())

# Display the shape of the dataframe:
print(df_train.shape)

# Display the column names and data types of the dataframe:
print(df_train.info())

# Display summary statistics for the numerical columns in the dataframe:
print(df_train.describe())

# Display the distribution of the target variable:
sns.histplot(df_train['target'], kde=True)
plt.show()

# Display the correlation matrix of the numerical features:
corr_matrix = df_train.corr()
sns.heatmap(corr_matrix, annot=True)
plt.show()

# Display boxplots of the numerical features:
num_cols = [f'{i}' for i in range(52)]
sns.boxplot(data=df_train[num_cols])
plt.xticks(rotation=90)
plt.show()

# Explore the data to get a better understanding of the features
# and the target variable:
print(df_train.head())
print(df_train.describe())
print(df_train.info())
print(df_train['target'].value_counts())

# Preprocess the data by handling missing values, scaling
# the numerical features, and encoding the categorical features:
# Handling missing values
df_train.fillna(df_train.mean(), inplace=True)
df_test.fillna(df_test.mean(), inplace=True)

# Split the data into training and validation sets:
X_train, X_val, y_train, y_val = train_test_split(df_train.drop('target', axis=1), 
                                                  df_train['target'], 
                                                  test_size=0.2, 
                                                  random_state=42)


# reg = LazyClassifier(verbose=0,ignore_warnings=False, custom_metric=None)
# clf = LazyClassifier(verbose=0,ignore_warnings=True, custom_metric=None)
# models, predictions = clf.fit(X_train, X_test, y_train, y_test)

# print("results:")
# print(models)


# Choose a suitable model based on the problem statement and
# the characteristics of the data. Train the model on the training
# set and evaluate its performance on the validation set using RMSE:
model = XGBRegressor(n_estimators=100, max_depth=5, learning_rate=0.1)
model.fit(X_train, y_train)

y_pred_train = model.predict(X_train)
y_pred_val = model.predict(X_val)

rmse_train = mean_squared_error(y_train, y_pred_train, squared=False)
rmse_val = mean_squared_error(y_val, y_pred_val, squared=False)

print('Training RMSE:', rmse_train)
print('Validation RMSE:', rmse_val)

# Tune the hyperparameters of the model using cross-validation
# techniques to improve its performance:
param_grid = {'n_estimators': [50, 100, 150],
              'max_depth': [3, 5, 7],
              'learning_rate': [0.1, 0.05, 0.01]}

grid_search = GridSearchCV(model, param_grid, cv=3, scoring='neg_mean_squared_error')
grid_search.fit(X_train, y_train)

print('Best parameters:', grid_search.best_params_)
print('Best score:', grid_search.best_score_)

# Once the model is optimized, use it to make predictions on the test dataset.
# Preprocess the test dataset in the same way as the training dataset:
# Handling missing values
df_test.fillna(df_test.mean(), inplace=True)

# Making predictions
y_pred_test = model.predict(df_test)

# Saving predictions to file
submission = pd.DataFrame({'target': y_pred_test})
submission.to_csv('task_03/data/predictions.csv', index=False)

