# Task 03: Regression on the tabular data
## Task:
You have a dataset (`internship_train.csv`) that contains 53 anonymized features and a target column. Your task is to build model that predicts a target based on the proposed features. Please provide predictions for `internship_hidden_test.csv` file. Target metric is `RMSE`. The main goal is to provide github repository that contains:
- jupyter notebook with analysis; 
- code for modeling (Python 3); 
- file with model predictions; 
- readme file;
- requirements.txt file.

## Solution:
### Script without Optuna
The code is used to solve a machine learning problem, specifically a regression problem. The problem involves predicting a continuous target variable from a set of input features.

The code starts by importing the necessary libraries required for data preprocessing, visualization, machine learning algorithms, and evaluation. NumPy and Pandas are used for data manipulation, Matplotlib and Seaborn for data visualization, Scikit-learn for machine learning algorithms, and XGBoost, HDBSCAN, and LazyPredict for model optimization. SHAP is used to explain the model's predictions.

Next, the code loads the training and test datasets from CSV files into Pandas dataframes using the read_csv() function. The datasets are stored as Pandas dataframes for easy manipulation and analysis.

The code then explores the training dataset to get a better understanding of the features and the target variable by using various Pandas functions such as head(), describe(), info(), and value_counts(). The head() function displays the first few rows of the dataset, describe() provides the summary statistics of the dataset, info() gives information about the dataset such as the number of rows and columns, and value_counts() counts the number of occurrences of each class in the target variable.

After exploring the dataset, the code separates the features and target variable from the training dataset using Pandas' loc[] function. The target variable is stored separately in the y_target variable, and the input features are stored in the x_target variable. The dataset is then split into training and validation sets using Scikit-learn's train_test_split() function. The test_size parameter is set to 0.20, indicating that 20% of the data is used for validation.

The code then uses XGBoost to fit a regression model to the training dataset.

SHAP is a model-agnostic tool used to explain the predictions of any machine learning model. It does this by calculating the contribution of each feature to the model's output. The SHAP values are then visualized using various plots such as waterfall, beeswarm, bar, and scatter plots.

SHAP Results

Waterfall plot

The bottom of the [waterfall](https://shap.readthedocs.io/en/latest/example_notebooks/api_examples/plots/waterfall.html) plot starts with the expected value of the model output, and then each line shows as positive (red) or the negative (blue) contribution of each feature moves the value from the expected model output over the background dataset to the model output for that prediction.

![Figure_1.png](figures/Figure_1.png)

Beeswarm summary plot

The [beeswarm](https://shap.readthedocs.io/en/latest/example_notebooks/api_examples/plots/beeswarm.html) plot is designed to provide an informative summary of how the main features in the dataset affect the output of the model. Each instance of a given explanation is represented by one dot on each feature stream. The x position of a point is determined by the SHAP value (`shap_values.value[instance,feature]`) of that function, and the points are "piled up" along each line of the function to show density. The color is used to display the original value of the feature (`shap_values.data[instance,feature]`).

![Figure_2.png](figures/Figure_2.png)

Global bar plot

Passing a matrix of SHAP values to the [histogram](https://shap.readthedocs.io/en/latest/example_notebooks/api_examples/plots/bar.html) function produces a global feature importance plot where the global importance of each feature is taken as the mean absolute value of this feature for all given samples.

![Figure_3.png](figures/Figure_3.png)

Scatter plot

[Dependency scatterplot](https://shap.readthedocs.io/en/latest/example_notebooks/api_examples/plots/scatter.html) shows the impact of a single feature on the predictions made by the model:
- Each point is one prediction (row) from the dataset.
- The X-axis is the function value (from the `X` matrix stored in `shap_values.data`).
- The y-axis is the SHAP value for this function (stored in `shap_values.values`), which indicates how much knowing the value of this function changes the model output for the prediction of this sample.
- The light gray area at the bottom of the graph is a histogram showing the distribution of data values.

Visualization of individual features of the original space with the greatest influence on the resulting value of the model prediction value is shown in the following figures:

Influence of feature "6"
![Figure_4.png](figures/Figure_4.png)

Influence of feature "7"
![Figure_5.png](figures/Figure_5.png)

As a result of applying SHAP, the following features were selected, which had the greatest impact on the resulting value of the model prediction value:
1. feature "6"
2. feature "7"

Next, the code trains an XGBoost regression model on the training set and evaluates its performance on the validation set using root mean squared error (RMSE). RMSE is a common metric used to evaluate regression models. It measures the difference between the predicted and actual values of the target variable. The code then tunes the hyperparameters of the model using GridSearchCV from Scikit-learn to improve its performance.

Finally, the code preprocesses the test dataset in the same way as the training dataset and makes predictions using the optimized XGBoost regression model. It saves the predictions to a CSV file for submission.

In summary, the code performs data exploration, preprocessing, visualization, machine learning model training and optimization, and prediction on the test dataset. The ultimate goal is to train a regression model that can accurately predict the target variable based on the input features.

Results:
- Training RMSE: 0.07349566519847942
- Validation RMSE: 0.07571904937384166
- Best parameters: {'learning_rate': 0.1, 'max_depth': 7, 'n_estimators': 150}
- Best score: -0.0002504847761641363

### Script with Optuna

This is a Python script that uses the Optuna library for hyperparameter optimization and the XGBoost library for machine learning.

The script starts by importing the required libraries, including Pandas for data manipulation, Optuna for hyperparameter optimization, XGBoost for machine learning, and scikit-learn for data preprocessing and evaluation.

The script then loads the training and test data from CSV files using the Pandas library. The training data is split into training and validation sets using the train_test_split function from scikit-learn.

The objective function for Optuna is then defined. This function takes a set of hyperparameters as input, trains an XGBoost model with these hyperparameters on the training data, and evaluates the model on the validation data using the mean squared error (MSE) metric. The objective function returns the root mean squared error (RMSE) of the model on the validation set.

The script then runs the optimization process using the create_study function from Optuna. The direction parameter is set to 'minimize' since we want to minimize the RMSE. The study.optimize function is called with n_trials=5 to run the optimization process for 5 trials.

After the optimization process is complete, the script trains an XGBoost model on the full training data using the best hyperparameters found by Optuna. The model is then evaluated on the training and validation sets using the mean squared error (MSE) metric, and the RMSE is printed to the console.

The test data is then preprocessed to fill in missing values with the mean, and the trained model is used to make predictions on the test data. The predictions are saved to a CSV file called 'predictions_optuna.csv' using the Pandas library.

Results:
- Training RMSE: 0.013468791754463937
- Validation RMSE: 0.014663132164823069